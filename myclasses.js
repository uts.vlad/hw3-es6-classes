class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    // consolidatedData () {return [this.name, this.age, this.salary]}  //!! это для себя (эксперименты)


    get getName ()  {return this.name}
    get getAge ()   {return this.age}
    get getSalary () {return this.salary}
    set setName (newName)  {this.name = newName}
    set setAge (newAge)   {this.age = newAge}
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
    super (name, age, salary);  // вызывает конструктор super класса Employee и передаёт параметры name, age, salary
    this.lang = lang;
    }
    get getSalary (){return this.salary * 3}

    // consolidatedData () {
    // super.consolidatedData();
    //     console.log('example of usage of super.');
    // }
}

const Vlad = new Programmer ('Vlad', 48, 500, ['rus', 'ukr', 'eng']);
const Blad = new Programmer ('Blad', 18, 150, ['rus', 'eng']);
const Plad = new Programmer ('Plad', 23, 250, ['eng']);

console.log(Vlad);
console.log(Blad);
console.log(Plad);





